/*
==============================================================================================================================================================

www.sourceforge.net/projects/sinip
Copyright Stephane Baudoux (www.stephane-baudoux.com)

This file is part of SiniP.

SiniP is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SiniP is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SiniP.  If not, see <http://www.gnu.org/licenses/>.

==============================================================================================================================================================
*/

#include "SiniPProperty.h"

/**
* Constructor
* @param key The key name
* @param value The value
*/
SiniPProperty::SiniPProperty(const std::string & key, const std::string & value)
{
	this->m_type = SINIP_ELEMENT_PROPERTY;
	this->m_key = key;
	SetValue(value);
}

/**
* Constructor
* @param key The key name
* @param value The value
* @param comment The comment
*/
SiniPProperty::SiniPProperty(const std::string & key, const std::string & value, const std::string & comment)
{
	this->m_type = SINIP_ELEMENT_PROPERTY;
	this->m_key = key;
	SetValue(value);
	SetComment(comment);
}

/**
* Set the value
* @param value The value
*/
void SiniPProperty::SetValue(const std::string & value)
{
	this->m_value = value;
}

/**
* Set the comment
* @param comment The comment
*/
void SiniPProperty::SetComment(const std::string & comment)
{
	this->m_comment = comment;
}

/**
* Get the key name
* @return The key name
*/
const std::string & SiniPProperty::GetKey() const
{
	return this->m_key;
}

/**
* Get the value
* @return The value
*/
const std::string & SiniPProperty::GetValue() const
{
	return this->m_value;
}

/**
* Get the comment
* @return The comment
*/
const std::string & SiniPProperty::GetComment() const
{
	return this->m_comment;
}

/**
* Return a string representation of the element
* @return A string representation of the element
*/
const std::string SiniPProperty::ToString() const
{
	std::string result = "";
	if(this->m_value != "")
	{
		result = this->m_key + " = " + this->m_value;
		if(this->m_comment != "")
			result += " " + this->m_comment;
	}

	return result;
}
