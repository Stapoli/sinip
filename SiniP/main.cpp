#include "SiniP.h"

#pragma comment(lib,"SiniP.lib")

int main()
{
	SiniP test("test.ini");

	// Getters
	bool tmp1 = test.GetValueBool("general", "fullscreen", true);
	int tmp2 = test.GetValueInt("mouse", "sensibility", 0);
	float tmp3 = test.GetValueFloat("general", "fov", 60);
	double tmp4 = test.GetValueDouble("music", "music", 50);
	std::string tmp5 = test.GetValueString("general", "resolution", "");
	test.SaveToFile("test_output.ini");

	// Setters
	SiniP test2;
	test2.SetValueBool("", "hide", true);
	test2.SetValueFloat("general", "atchoum", 3.0f);
	test2.SetValueBool("general", "awesome", true);
	test2.SetValueBool("general", "synchro", false);
	test2.SetValueInt("advanced", "volume", 7);
	test2.SetValueString("more_advanced", "path", "absolute");
	test2.Save();

	// Copy
	SiniP test3 = test;
	test3.Copy(test2);
	test3.RemoveAllComments();
	test3.SaveToFile("copy_test.ini");
	

	return 0;
}
