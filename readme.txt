/** @mainpage

<h1> SiniP </h1>

SiniP is a simple ini parser that allow you to open, manipulate and save ini files.

<h2> How it works: Read values </h2>
Reading a value is really easy. If a property value can't be converted in the chosen format or if the property doesn't exists, SiniP will return the default value.

@code

SiniP test("test.ini");

bool tmp1 = test.GetValueBool("general", "fullscreen", true);
int tmp2 = test.GetValueInt("mouse", "sensibility", 0);
float tmp3 = test.GetValueFloat("general", "fov", 60);
double tmp4 = test.GetValueDouble("music", "music", 50);
std::string tmp5 = test.GetValueString("general", "resolution", "");

@endcode

<h2> How it works: Write values </h2>

@code
SiniP test("test.ini");

test.SetValueFloat("general", "atchoum", 3.0f);
test.SetValueBool("general", "awesome", true);
test.SetValueInt("advanced", "volume", 7);
test.SetValueString("more_advanced", "path", "absolute");

test.Save(); // Replace the file
test.SaveToFile("duplicate.ini"); // Save to another file

@endcode

<h2> An example of a valid ini file for SiniP </h2>

@image html example.png

www.sourceforge.net/projects/sinip

Stephane Baudoux
*/